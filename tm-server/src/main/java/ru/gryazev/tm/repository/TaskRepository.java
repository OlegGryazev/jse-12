package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.enumerated.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull private final Connection connection;

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("taskName"));
        task.setDetails(row.getString("details"));
        task.setDateStart(row.getDate("dateStart"));
        task.setDateFinish(row.getDate("dateFinish"));
        task.setStatus(Status.valueOf(row.getString("status")));
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND id=?");
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=?");
        statement.setString(1, userId);
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task persist(@NotNull final String userId, @NotNull final Task task) throws SQLException {
        if (!userId.equals(task.getUserId())) return null;
        @Nullable final Date sqlDateStart = task.getDateStart() == null ?
                null : new Date(task.getDateStart().getTime());
        @Nullable final Date sqlDateFinish = task.getDateFinish() == null ?
                null : new Date(task.getDateFinish().getTime());

        @NotNull final PreparedStatement statement = connection
                .prepareStatement("INSERT INTO app_task VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
        statement.setString(1, task.getId());
        statement.setString(2, task.getProjectId());
        statement.setString(3, task.getUserId());
        statement.setString(4, task.getName());
        statement.setString(5, task.getDetails());
        statement.setDate(6, sqlDateStart);
        statement.setDate(7, sqlDateFinish);
        statement.setString(8, task.getStatus().toString());
        statement.setLong(9, task.getCreateMillis());
        statement.execute();
        statement.close();
        return task;
    }

    @NotNull
    @Override
    public Task merge(@NotNull final String userId, @NotNull final Task task) throws SQLException {
        @NotNull final PreparedStatement selectStatement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND id=?");
        selectStatement.setString(1, userId);
        selectStatement.setString(2, task.getId());
        if (selectStatement.executeQuery() == null) {
            selectStatement.close();
            persist(userId, task);
            return task;
        }
        selectStatement.close();
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("UPDATE app_task SET project_id=?, taskName=?, details=?, dateStart=?, dateFinish=?, status=? WHERE id=?");
        @Nullable final Date sqlDateStart = task.getDateStart() == null ?
                null : new Date(task.getDateStart().getTime());
        @Nullable final Date sqlDateFinish = task.getDateFinish() == null ?
                null : new Date(task.getDateFinish().getTime());
        statement.setString(1, task.getProjectId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDetails());
        statement.setDate(4, sqlDateStart);
        statement.setDate(5, sqlDateFinish);
        statement.setString(6, task.getStatus().toString());
        statement.setString(7, task.getId());
        statement.execute();
        statement.close();
        return task;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_task WHERE id=? AND user_id=?");
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_task WHERE user_id=? AND project_id=?");
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_task WHERE user_id=?");
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND project_id=?");
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @Override
    public @NotNull List<Task> findUnlinked(@NotNull String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND project_id IS NULL");
        statement.setString(1, userId);
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findTaskByIndex(@NotNull String projectId, @NotNull String userId, int taskIndex) throws SQLException {
        return findTasksByProjectId(userId, projectId).stream().skip(taskIndex).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findUnlinkedTaskByIndex(@NotNull String userId, int taskIndex) throws SQLException {
        return findUnlinked(userId).stream().skip(taskIndex).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findByName(@NotNull final String userId, @NotNull final String taskName) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND taskName LIKE ?");
        statement.setString(1, userId);
        statement.setString(2, "%" + taskName + "%");
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByDetails(@NotNull final String userId, @NotNull final String taskDetails) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_task WHERE user_id=? AND details LIKE ?");
        statement.setString(1, userId);
        statement.setString(2, "%" + taskDetails + "%");
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection.prepareStatement("SELECT * FROM app_task");
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_task");
        statement.execute();
        statement.close();
    }

    @Override
    public @NotNull List<Task> findTasksByProjectIdSorted(@NotNull String userId, @NotNull String projectId, @NotNull String sqlSortType) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id=? AND project_id=? ORDER BY " + sqlSortType;
        @NotNull final PreparedStatement statement = connection
                .prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    @Override
    public @NotNull List<Task> findUnlinkedSorted(@NotNull String userId, @NotNull String sqlSortType) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id=? AND project_id=null ORDER BY " + sqlSortType;
        @NotNull final PreparedStatement statement = connection
                .prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final List<Task> tasks = findByPreparedStatement(statement);
        statement.close();
        return tasks;
    }

    private @NotNull List<Task> findByPreparedStatement(@NotNull final PreparedStatement statement) throws SQLException {
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next())
            tasks.add(fetch(resultSet));
        resultSet.close();
        return tasks;
    }

}
