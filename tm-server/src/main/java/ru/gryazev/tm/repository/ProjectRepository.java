package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.enumerated.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final Connection connection;

    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("projectName"));
        project.setDetails(row.getString("details"));
        project.setDateStart(row.getDate("dateStart"));
        project.setDateFinish(row.getDate("dateFinish"));
        project.setStatus(Status.valueOf(row.getString("status")));
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM app_project WHERE user_id=? AND id=?");
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @NotNull
    @Override
    public Project merge(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final PreparedStatement selectStatement = connection
                .prepareStatement("SELECT * FROM app_project WHERE user_id=? AND id=?");
        selectStatement.setString(1, userId);
        selectStatement.setString(2, project.getId());
        if (selectStatement.executeQuery() == null) {
            selectStatement.close();
            persist(userId, project);
            return project;
        }
        selectStatement.close();
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("UPDATE app_project SET projectName=?, details=?, dateStart=?, dateFinish=?, status=? WHERE id=?");
        @Nullable final Date sqlDateStart = project.getDateStart() == null ?
                null : new Date(project.getDateStart().getTime());
        @Nullable final Date sqlDateFinish = project.getDateFinish() == null ?
                null : new Date(project.getDateFinish().getTime());
        statement.setString(1, project.getName());
        statement.setString(2, project.getDetails());
        statement.setDate(3, sqlDateStart);
        statement.setDate(4, sqlDateFinish);
        statement.setString(5, project.getStatus().toString());
        statement.setString(6, project.getId());
        statement.execute();
        statement.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) throws SQLException {
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM app_project WHERE user_id=?");
        statement.setString(1, userId);
        @NotNull final List<Project> projects = findByPreparedStatement(statement);
        statement.close();
        return projects;
    }

    @Nullable
    @Override
    public Project persist(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        if (!userId.equals(project.getUserId())) return null;
        @Nullable final Date sqlDateStart = project.getDateStart() == null ?
                null : new Date(project.getDateStart().getTime());
        @Nullable final Date sqlDateFinish = project.getDateFinish() == null ?
                null : new Date(project.getDateFinish().getTime());

        @NotNull final PreparedStatement statement = connection
                .prepareStatement("INSERT INTO app_project VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        statement.setString(1, project.getId());
        statement.setString(2, project.getUserId());
        statement.setString(3, project.getName());
        statement.setString(4, project.getDetails());
        statement.setDate(5, sqlDateStart);
        statement.setDate(6, sqlDateFinish);
        statement.setString(7, project.getStatus().toString());
        statement.setLong(8, project.getCreateMillis());
        statement.execute();
        statement.close();
        return project;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_project WHERE id=? AND user_id=?");
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_project WHERE user_id=?");
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    public List<Project> findByName(@NotNull final String userId, @NotNull final String projectName) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_project WHERE user_id=? AND projectName LIKE ?");
        statement.setString(1, userId);
        statement.setString(2, "%" + projectName + "%");
        @NotNull final List<Project> projects = findByPreparedStatement(statement);
        statement.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDetails(@NotNull final String userId, @NotNull final String projectDetails) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_project WHERE user_id=? AND details LIKE ?");
        statement.setString(1, userId);
        statement.setString(2, "%" + projectDetails + "%");
        @NotNull final List<Project> projects = findByPreparedStatement(statement);
        statement.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAllSorted(@NotNull final String userId, @NotNull final String sqlSortType) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id=? ORDER BY " + sqlSortType;
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final List<Project> projects = findByPreparedStatement(statement);
        statement.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findProjectByIndex(@NotNull final String userId, final int index) throws SQLException {
        return findAll(userId).stream().skip(index).findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection.prepareStatement("SELECT * FROM app_project");
        @NotNull final List<Project> projects = findByPreparedStatement(statement);
        statement.close();
        return projects;
    }

    @NotNull
    private List<Project> findByPreparedStatement(@NotNull final PreparedStatement statement) throws SQLException {
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next())
            projects.add(fetch(resultSet));
        resultSet.close();
        return projects;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_project");
        statement.execute();
        statement.close();
    }

}
