package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull private final Connection connection;

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPwdHash(row.getString("pwdHash"));
        user.setRoleType(RoleType.valueOf(row.getString("role")));
        user.setName(row.getString("name"));
        return user;
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_user WHERE id=?");
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_user");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> users = new ArrayList<>();
        while (resultSet.next())
            users.add(fetch(resultSet));
        resultSet.close();
        statement.close();
        return users;
    }

    @Nullable
    @Override
    public User persist(@NotNull final String userId, @NotNull final User user) throws SQLException {
        if (!userId.equals(user.getUserId())) return null;
        @Nullable final String role = user.getRoleType() == null ?
                null : user.getRoleType().toString();

        @NotNull final PreparedStatement statement = connection
                .prepareStatement("INSERT INTO app_user VALUES(?, ?, ?, ?, ?)");
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPwdHash());
        statement.setString(4, user.getName());
        statement.setString(5, role);
        statement.execute();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    public User merge(@NotNull final String userId, @NotNull final User user) throws SQLException {
        @NotNull final PreparedStatement selectStatement = connection
                .prepareStatement("SELECT * FROM app_user WHERE id=?");
        selectStatement.setString(1, userId);
        if (selectStatement.executeQuery() == null) {
            selectStatement.close();
            persist(userId, user);
            return user;
        }
        selectStatement.close();
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("UPDATE app_user SET login=?, pwdHash=?, name=?, role=? WHERE id=?");
        @Nullable final String role = user.getRoleType() == null ?
                null : user.getRoleType().toString();
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPwdHash());
        statement.setString(3, user.getName());
        statement.setString(4, role);
        statement.setString(5, user.getId());
        statement.execute();
        statement.close();
        return user;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statementForSession = connection
                .prepareStatement("DELETE FROM app_session WHERE user_id=?");
        statementForSession.setString(1, id);
        statementForSession.execute();
        statementForSession.close();
        @NotNull final PreparedStatement statementForUser = connection
                .prepareStatement("DELETE FROM app_user WHERE id=?");
        statementForUser.setString(1, id);
        statementForUser.execute();
        statementForUser.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statementForSession = connection
                .prepareStatement("DELETE FROM app_session");
        statementForSession.execute();
        statementForSession.close();
        @NotNull final PreparedStatement statementForUser = connection
                .prepareStatement("DELETE FROM app_user");
        statementForUser.execute();
        statementForUser.close();
    }

    @Nullable
    @Override
    public User findByLoginAndPwd(@NotNull final String login, @NotNull final String pwd) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_user WHERE login=? AND pwdHash=?");
        statement.setString(1, login);
        statement.setString(2, pwd);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    public User findUserByIndex(final int userIndex) throws SQLException {
        return findAll().stream().skip(userIndex).findFirst().orElse(null);
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement statementForSession = connection
                .prepareStatement("DELETE FROM app_session");
        statementForSession.execute();
        statementForSession.close();
        @NotNull final PreparedStatement statementForUser = connection
                .prepareStatement("DELETE FROM app_user");
        statementForUser.execute();
        statementForUser.close();
    }

}
