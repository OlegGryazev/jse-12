package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.sql.SQLException;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractCrudEntity> implements IRepository<T> {

    @Nullable
    public abstract T findOne(@NotNull String userId, @NotNull String id) throws SQLException;

    @Nullable
    public abstract T persist(@NotNull String userId, @NotNull T t) throws SQLException;

    public abstract void remove(@NotNull String userId, @NotNull String id) throws SQLException;

    public abstract void removeAll(@NotNull String userId) throws SQLException;

    @NotNull
    public abstract T merge(@NotNull String userId, @NotNull T t) throws Exception;

    @NotNull
    public abstract List<T> findAll() throws SQLException;

    public abstract void removeAll() throws SQLException;

}
