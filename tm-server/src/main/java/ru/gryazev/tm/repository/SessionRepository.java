package ru.gryazev.tm.repository;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final Connection connection;

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setUserId(row.getString("user_id"));
        session.setRole(RoleType.valueOf(row.getString("role")));
        session.setSignature(row.getString("signature"));
        return session;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_session WHERE user_id=? AND id=?");
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.first();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("SELECT * FROM app_session WHERE user_id=?");
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next())
            sessions.add(fetch(resultSet));
        resultSet.close();
        statement.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session persist(@NotNull final String userId, @NotNull final Session session) throws SQLException {
        if (!userId.equals(session.getUserId())) return null;
        @Nullable final String role = session.getRole() == null ?
                null : session.getRole().toString();

        @NotNull final PreparedStatement statement = connection
                .prepareStatement("INSERT INTO app_session VALUES(?, ?, ?, ?, ?)");
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, role);
        statement.setString(4, session.getSignature());
        statement.setLong(5, session.getTimestamp());
        statement.execute();
        statement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_session WHERE id=? AND user_id=?");
        statement.setString(1, id);
        statement.setString(2, userId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_session WHERE user_id=?");
        statement.setString(1, userId);
        statement.execute();
        statement.close();
    }

    @NotNull
    @Override
    public Session merge(@NotNull final String userId, @NotNull final Session session) throws Exception {
        throw new Exception("Session edit is not allowed");
    }

    @NotNull
    @Override
    public List<Session> findAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection.prepareStatement("SELECT * FROM app_session");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next())
            sessions.add(fetch(resultSet));
        resultSet.close();
        statement.close();
        return sessions;
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final PreparedStatement statement = connection
                .prepareStatement("DELETE FROM app_session");
        statement.execute();
        statement.close();
    }

}
