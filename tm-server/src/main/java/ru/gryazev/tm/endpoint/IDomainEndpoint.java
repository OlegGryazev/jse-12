package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    public void saveJsonJaxb(@Nullable Session session) throws Exception;

    public void saveXmlJaxb(@Nullable Session session) throws Exception;

    public void saveJsonFasterxml(@Nullable Session session) throws Exception;

    public void saveXmlFasterxml(@Nullable Session session) throws Exception;

    public void saveSer(@Nullable Session session) throws Exception;

    public void loadJsonJaxb(@Nullable Session session) throws Exception;

    public void loadXmlJaxb(@Nullable Session session) throws Exception;

    public void loadJsonFasterxml(@Nullable Session session) throws Exception;

    public void loadXmlFasterxml(@Nullable Session session) throws Exception;

    public void loadSer(@Nullable Session session) throws Exception;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator);

}
