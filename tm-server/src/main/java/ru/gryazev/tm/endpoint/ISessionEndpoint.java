package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;

import javax.jws.WebService;
import java.sql.SQLException;

@WebService
public interface ISessionEndpoint {

    @Nullable
    public Session findOneSession(@Nullable Session session, @NotNull String userId, @NotNull String id) throws Exception;

    public void removeSession(@NotNull Session session) throws Exception;

    @Nullable
    public Session createSession(@Nullable String login, @Nullable String pwdHash) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}
