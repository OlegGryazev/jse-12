package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.api.service.IService;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractService <T extends AbstractCrudEntity> implements IService<T> {

    @Nullable
    @Override
    public T create(@Nullable final String userId, @Nullable final T t) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(t)) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IRepository<T> repository = getRepository(connection);
        try{
            repository.persist(userId, t);
            connection.commit();
            return t;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public T findOne(@Nullable final String userId, @Nullable final String entityId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<T> repository = getRepository(connection);
        @Nullable final T t = repository.findOne(userId, entityId);
        connection.close();
        return t;
    }

    @Nullable
    @Override
    public T edit(@Nullable final String userId, @Nullable final T t) throws Exception {
        if (!isEntityValid(t)) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IRepository<T> repository = getRepository(connection);
        try{
            repository.merge(userId, t);
            connection.commit();
            return t;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String entityId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (entityId == null || entityId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IRepository<T> repository = getRepository(connection);
        try{
            repository.remove(userId, entityId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IRepository<T> repository = getRepository(connection);
        try{
            repository.removeAll(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IRepository<T> repository = getRepository(connection);
        try{
            repository.removeAll();
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<T> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final List<T> ts = getRepository(connection).findAll();
        connection.close();
        return ts;
    }

    @Contract("null -> false")
    public abstract boolean isEntityValid(@Nullable final T t);

    @NotNull
    public abstract IRepository<T> getRepository(@NotNull final Connection connection) throws SQLException;

    @NotNull
    public abstract Connection getConnection() throws Exception;

}
