package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.IConnectionService;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.CompareUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    final IConnectionService connectionService;

   @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex) throws Exception {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final Project project = new ProjectRepository(connection).findProjectByIndex(userId, projectIndex);
        connection.close();
        if (project == null) return null;
        return project.getId();
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projects = new ProjectRepository(connection).findByName(userId, projectName);
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projects = new ProjectRepository(connection).findByDetails(userId, projectDetails);
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projects = new ProjectRepository(connection).findAllSorted(userId, sqlSortType);
        connection.close();
        return projects;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try{
            projectRepository.removeAll(userId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final List<Project> list = new ArrayList<>(repository.findAll(userId));
        connection.close();
        return list;
    }

    @Override
    public boolean isEntityValid(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        if (project.getName() == null || project.getName().isEmpty()) return false;
        return project.getUserId() != null && !project.getUserId().isEmpty();
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final Connection connection) throws SQLException {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return connectionService.getConnection();
    }

}
