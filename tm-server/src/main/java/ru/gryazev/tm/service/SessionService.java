package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IConnectionService;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.SessionRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.util.SignatureUtil;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Date;

@AllArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    final IConnectionService connectionService;
    
    @Nullable
    @Override
    public Session create(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (pwdHash == null || pwdHash.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IUserRepository userRepository = new UserRepository(connection);
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        @Nullable final User user = userRepository.findByLoginAndPwd(login, pwdHash);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setRole(user.getRoleType());
        session.setUserId(user.getId());
        session.setSignature(SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT));
        try {
            sessionRepository.persist(user.getId(), session);
            connection.commit();
            return session;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean isEntityValid(@Nullable final Session session) {
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty()) return false;
        if (session.getTimestamp() == 0) return false;
        return (session.getSignature() != null && !session.getSignature().isEmpty());
    }

    public void validateSession(@Nullable final Session session, @Nullable RoleType[] roles) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (roles == null || !Arrays.asList(roles).contains(session.getRole())) throw new Exception(message);
        validateSession(session);
    }

    public void validateSession(@Nullable final Session session) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty()) throw new Exception(message);
        @NotNull final long sessionAliveTime = new Date().getTime() - session.getTimestamp();
        if (sessionAliveTime > Constant.SESSION_LIFETIME) throw new Exception(message);
        if (session.getRole() == null) throw new Exception(message);
        @Nullable final Session sessionAtServer = findOne(session.getUserId(), session.getId());
        if (sessionAtServer == null || sessionAtServer.getSignature() == null) throw new Exception(message);
        @Nullable final String actualSessionSignature =  SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT);
        if (!sessionAtServer.getSignature().equals(actualSessionSignature)) throw new Exception(message);
    }

    @NotNull
    @Override
    public ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return connectionService.getConnection();
    }

}
