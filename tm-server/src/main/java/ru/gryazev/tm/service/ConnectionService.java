package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.IConnectionService;
import ru.gryazev.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull final IPropertyService propertyService = new PropertyService();

    @NotNull
    public Connection getConnection() throws Exception {
        @NotNull final String url = propertyService.getProperty("url");
        @NotNull final String username = propertyService.getProperty("username");
        @NotNull final String password = propertyService.getProperty("password");
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        return connection;
    }

}
