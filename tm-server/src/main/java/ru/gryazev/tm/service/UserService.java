package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IConnectionService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.repository.UserRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

@AllArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @Getter
    @NotNull
    final IConnectionService connectionService;
    
    public String login(@Nullable final User user) throws Exception {
        if (!isEntityValid(user)) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        final User loggedUser = repository.findByLoginAndPwd(user.getLogin(), user.getPwdHash());
        if (loggedUser == null) return null;
        return loggedUser.getId();
    }

    public boolean checkRole(@Nullable final String userId, @Nullable final RoleType[] roles) throws Exception {
        if (roles == null) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        @Nullable final User user = repository.findOne(userId, userId);
        if (user == null) return false;
        return Arrays.asList(roles).contains(user.getRoleType());
    }

    @Nullable
    @Override
    public String getUserId(int userIndex) throws Exception {
        if (userIndex < 0) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        User user = repository.findUserByIndex(userIndex);
        if (user == null) return null;
        return user.getId();
    }

    @Override
    public boolean isEntityValid(@Nullable final User user) {
        if (user == null || user.getRoleType() == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        return user.getPwdHash() != null && !user.getPwdHash().isEmpty();
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull final Connection connection) throws SQLException {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return connectionService.getConnection();
    }
    
}
