package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.IConnectionService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.util.CompareUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    final IConnectionService connectionService;
    
    @NotNull
    @Override
    public List<Task> listTaskUnlinked(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final List<Task> tasks = repository.findUnlinked(userId);
        connection.close();
        return tasks;
    }

    @Override
    @SneakyThrows
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            repository.removeByProjectId(userId, projectId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Task unlinkTask(@Nullable final String userId, @Nullable final Task task) throws Exception {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            task.setProjectId(null);
            @Nullable final Task unlinkedTask = repository.merge(userId, task);
            connection.commit();
            return unlinkedTask;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @Nullable final Task task = repository.findTaskByIndex(projectId, userId, taskIndex);
        connection.close();
        if (task == null) return null;
        return task.getId();
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final List<Task> tasks = repository.findTasksByProjectId(userId, projectId);
        connection.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task linkTask(@Nullable final String userId,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (newProjectId == null || newProjectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = getRepository(connection);
        @Nullable final Task task = oldProjectId == null ?
                repository.findUnlinkedTaskByIndex(userId, taskIndex) :
                repository.findTaskByIndex(oldProjectId, userId, taskIndex);
        if (task == null) return null;
        task.setProjectId(newProjectId);
        try {
            @Nullable final Task linkedTask = repository.merge(userId, task);
            connection.commit();
            return linkedTask;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception(e);
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String userId, @Nullable final String taskName) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final List<Task> tasks = repository.findByName(userId, taskName);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final List<Task> tasks = repository.findByDetails(userId, taskDetails);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final List<Task> tasks = repository.findTasksByProjectIdSorted(userId, projectId, sqlSortType);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final List<Task> tasks = repository.findUnlinkedSorted(userId, sqlSortType);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<Task> list = new ArrayList<>(repository.findAll(userId));
        connection.close();
        return list;
    }

    @Override
    public boolean isEntityValid(@Nullable final Task task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUserId() == null || task.getUserId().isEmpty()) return false;
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    public Connection getConnection() throws Exception {
        return connectionService.getConnection();
    }

}