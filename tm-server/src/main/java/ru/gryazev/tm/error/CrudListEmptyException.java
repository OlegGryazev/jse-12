package ru.gryazev.tm.error;

public final class CrudListEmptyException extends CrudException {

    public CrudListEmptyException() {
        super("Error: list is empty");
    }

}
