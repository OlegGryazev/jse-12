package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.AbstractCrudEntity;
import ru.gryazev.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<T extends AbstractCrudEntity> {

    @Nullable
    public T findOne(@NotNull String userId, @NotNull String id) throws SQLException;

    @Nullable
    public T persist(@NotNull String userId, @NotNull T t) throws SQLException;

    public void remove(@NotNull String userId, @NotNull String id) throws SQLException;

    public void removeAll(@NotNull String userId) throws SQLException;

    @NotNull
    public T merge(@NotNull String userId, @NotNull T t) throws Exception;

    @NotNull
    public List<T> findAll() throws SQLException;

    public void removeAll() throws SQLException;

}
