package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IRepository;
import ru.gryazev.tm.entity.AbstractCrudEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IService<T extends AbstractCrudEntity> {

    @Nullable
    public T create(@Nullable String userId, @Nullable T t) throws Exception;

    @Nullable
    public T findOne(@Nullable String userId, @Nullable String entityId) throws Exception;

    @Nullable
    public T edit(@Nullable String userId, @Nullable T t) throws Exception;

    public void remove(@Nullable String userId, @Nullable String entityId) throws Exception;

    public void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    public IRepository<T> getRepository(@NotNull Connection connection) throws SQLException;

    public void removeAll() throws Exception;

    @NotNull
    public List<T> findAll() throws Exception;

}
