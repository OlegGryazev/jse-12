package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    public List<Session> findAll(@NotNull String userId) throws SQLException;

}
