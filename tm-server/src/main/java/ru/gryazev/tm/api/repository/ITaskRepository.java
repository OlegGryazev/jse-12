package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    public List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;

    @NotNull
    public List<Task> findTasksByProjectIdSorted(@NotNull String userId,
                                           @NotNull String projectId,
                                           @NotNull String sqlSortType) throws SQLException;

    @NotNull
    public List<Task> findUnlinkedSorted(@NotNull String userId, @NotNull String sqlSortType) throws SQLException;

    @NotNull
    public List<Task> findUnlinked(@NotNull String userId) throws SQLException;

    @Nullable
    public Task findTaskByIndex(@NotNull String projectId, @NotNull String userId, int taskIndex) throws SQLException;

    @Nullable
    public Task findUnlinkedTaskByIndex(@NotNull String userId, int taskIndex) throws SQLException;

    @NotNull
    public List<Task> findByName(@NotNull String userId, @NotNull String taskName) throws SQLException;

    @NotNull
    public List<Task> findByDetails(@NotNull String userId, @NotNull String taskDetails) throws SQLException;

    @NotNull
    public List<Task> findAll(@NotNull String userId) throws SQLException;

    public void removeByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

}
