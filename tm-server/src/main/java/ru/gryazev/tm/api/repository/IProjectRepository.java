package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    public List<Project> findByName(@NotNull String userId, @NotNull String projectName) throws SQLException;

    @NotNull
    public List<Project> findByDetails(@NotNull String userId, @NotNull String projectDetails) throws SQLException;

    @Nullable
    public Project findProjectByIndex(@NotNull String userId, int index) throws SQLException;

    @NotNull
    public List<Project> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    public List<Project> findAllSorted(@NotNull String userId, @NotNull String sqlSortType) throws SQLException;

}
