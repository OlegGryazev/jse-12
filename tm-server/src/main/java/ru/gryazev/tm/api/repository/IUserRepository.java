package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    public User findByLoginAndPwd(@NotNull String login, @NotNull String pwd) throws SQLException;

    @NotNull
    public List<User> findAll() throws SQLException;

    @Nullable
    public User findUserByIndex(int userIndex) throws SQLException;

}
