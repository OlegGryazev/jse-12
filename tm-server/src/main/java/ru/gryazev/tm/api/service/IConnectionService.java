package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    public Connection getConnection() throws Exception;

}
