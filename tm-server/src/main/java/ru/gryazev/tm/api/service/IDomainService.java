package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;

public interface IDomainService {

    public void saveJsonJaxb() throws Exception;

    public void saveXmlJaxb() throws Exception;

    public void saveJsonFasterxml() throws Exception;

    public void saveXmlFasterxml() throws Exception;

    public void saveSer() throws Exception;

    public void loadJsonJaxb() throws Exception;

    public void loadXmlJaxb() throws Exception;

    public void loadJsonFasterxml() throws Exception;

    public void loadXmlFasterxml() throws Exception;

    public void loadSer() throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}
