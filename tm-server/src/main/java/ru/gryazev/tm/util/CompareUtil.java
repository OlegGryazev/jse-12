package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CompareUtil {

    @NotNull
    public static String getSqlSortType(@Nullable final String sortType) {
        if (sortType == null) return "timestamp";
        switch (sortType) {
            case "default": return "timestamp";
            case "date-start": return "dateStart";
            case "date-finish": return "dateFinish";
            case "status": return "status";
        }
        return "timestamp";
    }

}
