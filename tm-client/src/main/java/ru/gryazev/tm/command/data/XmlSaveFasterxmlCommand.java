package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;
import ru.gryazev.tm.endpoint.Session;

public class XmlSaveFasterxmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save-xml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save all data in XML format using Fasterxml.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final Session session = getSession();
        serviceLocator.getDomainEndpoint().saveXmlFasterxml(session);
        terminalService.print("[OK]");
    }

}
