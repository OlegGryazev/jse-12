package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final Session session = getSession();
        @Nullable String currentProjectId = getProjectId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskEndpoint.getTaskId(session, currentProjectId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @NotNull final Task task = terminalService.getTaskFromConsole();
        task.setId(taskId);
        task.setProjectId(currentProjectId);
        task.setUserId(session.getUserId());
        @Nullable final Task editedTask = taskEndpoint.editTask(session, task);
        if (editedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
