package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user data.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final Session session = getSession();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        terminalService.print("[USER EDIT]");
        @Nullable final User userEditData = terminalService.getUserPwdRepeat();
        if (userEditData == null) return;

        @Nullable final User currentUser = userEndpoint.findOneUser(session, session.getUserId());
        if (currentUser == null) throw new CrudUpdateException();
        userEditData.setRoleType(currentUser.getRoleType());
        userEditData.setId(currentUser.getId());
        @Nullable final User editedUser = userEndpoint.editUser(session, userEditData);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
