package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@NoArgsConstructor
public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null || settingService == null) return;
        @NotNull final Session session = getSession();
        @Nullable final String projectId = getCurrentProjectId();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String sortType = settingService.findValueByKey("task-sort");
        @NotNull final List<Task> tasks = taskEndpoint.findTaskByProjectSorted(session, projectId, sortType);
        if (tasks.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
