package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Session;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public class UserRemoveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null || sessionLocator == null) return;
        @Nullable final Session session = getSession();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        serviceLocator.getTaskEndpoint().removeAllTask(session);
        serviceLocator.getProjectEndpoint().removeAllProject(session);
        userEndpoint.removeUser(session, session.getUserId());
        sessionLocator.setSession(null);
        terminalService.print("[OK]");
    }

}
